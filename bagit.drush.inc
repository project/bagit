<?php

/**
 * @file
 * Drush integration file for the BagIt module.
 *
 * Usage: drush [--user=UID] create-bag NID
 *   where UID is the user ID or user name, and NID is the node ID
 *   of the node. Specifying a user is optional.
 */

/**
 * Implements hook_drush_help().
 */
function bagit_drush_help($command) {
  switch ($command) {
    case 'drush:create-bag':
      return dt('Create a Bag for a node');
  }
}

/**
 * Implements hook_drush_command().
 */
function bagit_drush_command() {
  $items = array();
  $items['create-bag'] = array(
    'description' => dt('Creates a Bag for a node.'),
    'arguments'   => array(
      'NID'    => dt('The node ID for the node you want to create a Bag for.'),
    ),
    'examples' => array(
      'Standard example' => 'drush --user=admin create-bag 190',
      'Alias example' => 'drush --user=admin cib 190',
    ),
    'aliases' => array('cib'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
  );
  return $items;
}

/**
 * Callback function for drush create-bag. 
 *
 * @param string $nid
 *   The node ID of the node to create a Bag for.
 */
function drush_bagit_create_bag($nid = NULL) {
  // Validate the arguments.
  if (!$nid) {
    drush_set_error('Node ID not specified', "Sorry, you need to supply a node ID.");
    return FALSE;
  }

  if ($nid) {
    try {
      $node = node_load($nid);
      bagit_create_bag($node);
    }
    catch (Exception $e) {
      drush_print("Sorry, cannot create the Bag: " . $e->getMessage());
    }
  }
}

